import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { MensajeService } from "~/domain/mensaje.service";
import { NoticiasService } from "~/domain/noticias.service";

@Component({
    selector: "News",
    templateUrl: "./news.component.html"
})

export class NewsComponent implements OnInit {

    constructor(private mensaje: MensajeService) {        
    }

    ngOnInit(): void {
        this.mensaje.crearMensaje("mensaje usuario");
        this.mensaje.crearMensaje("mensaje usuario 2");
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onItemtap(x): void {
        console.dir(x);
    }
}
