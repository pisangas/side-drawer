import { Injectable } from "@angular/core";

@Injectable()
export class NoticiasService {
    private Noticias: Array <string> = [];

    agregar(s: string){
        this.Noticias.push(s);
    }

    buscar(){
        return this.Noticias;
    }
}