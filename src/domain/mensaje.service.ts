import { Injectable } from "@angular/core";

@Injectable()
export class MensajeService {
    private Mensaje: Array <string> = [];

    crearMensaje (s: string){
        this.Mensaje.push(s);
    }

    mostrarMensaje(){
        return this.Mensaje;
    }
}